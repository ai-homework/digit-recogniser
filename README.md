# Digit Recogniser
----

Practice creating your own models using common libraries such as *Keras* and *PyTorch*.

## Getting Started
Instruction on how to train your own model has been provided in the .ppt at the root folder. Feel free to use cloud based platforms such as [*Google Colab**](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwi5wrD-r5vnAhXsH7cAHey2AtEQFjAAegQIBxAC&url=https%3A%2F%2Fcolab.research.google.com%2F&usg=AOvVaw3A5aPK2kLFzKOzb6sOckVw)

After saving your model, do try to run the it against your own handwriting to test the accuracy of your model. To load an image:
```
import cv2
img_path=r''
img=cv2.imread(img_path)
```

Use the `img` to store your handwriting and run it on the model.

----
If you have any questions, feel free to email me at <bilguun.batbold@nus.edu.sg>